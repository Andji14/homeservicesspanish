import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';


import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class FeedFeatured extends Component {
    static navigationOptions = {
        title: 'FeedFeatured',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>
                <Header moreOption={true}
                  onPressUpcoming={() => {
                    this.props.navigation.navigate('FeedUpcoming');
                  }}
                  pressMenu={() => {
                    this.props.navigation.navigate('NotificationScr');
                  }}
                />


                {/* CONTAINER */}
                <ScrollView>
                    <View style={{ width: width , height: height - 135, backgroundColor: 'white', alignItems: 'center', marginTop: 20,borderColor:'black', borderWidth:1  }}>
                        <Image source={require('./img/image.png')} style={styles.absolute} />


                        {/*BUTTONS */}
                        <View style={{ width: width-20, height: 40, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                            <TouchableOpacity style={{
                                width: (width/3) - 10,
                                height: 30,
                                borderRadius: 15,
                                backgroundColor: '#004ca0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginRight: 5
                            }}>
                                <Text style={{ fontSize: 10, color: 'white', fontFamily: 'Montserrat-Light',textAlign:'center' }}>SUMINISTRO DE AGUA</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{
                                width: (width/3) - 10,
                                height: 30,
                                borderRadius: 15,
                                backgroundColor: '#004ca0',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginRight: 5
                            }}>
                                <Text style={{ fontSize: 10, color: 'white', fontFamily: 'Montserrat-Light',textAlign:'center'  }}>FONTANERÍA</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{
                                width: (width/3) - 10,
                                height: 30,
                                borderRadius: 15,
                                backgroundColor: '#004ca0',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                <Text style={{ fontSize: 10, color: 'white', fontFamily: 'Montserrat-Light',textAlign:'center'  }}>BAÑERAS</Text>
                            </TouchableOpacity>
                        </View>


                        <View style={{ height: 60, width: width , marginLeft: 20, marginTop: 200 }}>
                            <Text style={{ backgroundColor: 'transparent', fontSize: 25, color: 'white', fontFamily: 'Montserrat-Light' }}>Marble Arch Repairs Ltd.</Text>
                            <View>
                                <Text style={{ backgroundColor: 'transparent',fontSize: 15, color: 'white', fontFamily: 'Montserrat-Light' }}>Calle Aduana,2 MADRID SPAIN</Text>
                                <Image source={require('./img/current copy 3.png')} style={{ width: 72, height: 15, position: 'absolute', top: 10, left: -13 }} />
                            </View>
                        </View>

                        <View style={{ height: 80, width: width, flexDirection: 'row', justifyContent: 'flex-end',}}>
                            <TouchableOpacity>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Image resizeMode='contain' source={require('./img/Ellipse 28 copy 2.png')} style={{ width: 75, display: 'flex', alignItems:'center', justifyContent: 'center'}}>
                                      <Text style={{ backgroundColor: 'transparent',fontSize: 11, color: 'black', fontFamily: 'Montserrat-Light'}}>ORDENAR</Text>
                                    </Image>
                                </View>
                            </TouchableOpacity>
                        </View>


                        <View style={{ height: 30, width: width - 20, flexDirection: 'row', marginLeft: 20 }}>
                            <View style={{ flexDirection: 'row', marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity>
                                    <Image source={require('./img/like_icon.png')} style={{ width: 20, height: 20, marginRight: 5 }} />
                                </TouchableOpacity>
                                <Text style={{ fontSize: 12, color: 'black' }}>45</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginRight: 10, justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity>
                                    <Image source={require('./img/comment_icon.png')} style={{ width: 20, height: 20, marginRight: 5 }} />
                                </TouchableOpacity>
                                <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Montserrat-Light' }}>OPINIONES</Text>
                            </View>
                        </View>



                        <View style={{ height: 40, width: width - 20, flexDirection: 'row', marginLeft: 20, }}>
                            <Text style={{ fontSize: 11, color: 'black', fontFamily: 'Montserrat-Light' }}>El Marble Arch Repairs Ltd. es un popular en todo Londres por su</Text>
                        </View>

                    </View>
                </ScrollView>

                   {/* FOOTER */}
                   <Footer icon={['user', 'home']} hightlightedIcon="home"  navigationType="navigate"
                     userPress={() => {
                       this.props.navigation.navigate('MyProfile');
                     }}
                     bellPress={() => {
                       this.props.navigation.navigate('Home');
                     }}
                   />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 5
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 370
    },
    footerContainer: {
        width: width,
        height: 45,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    }
});
