import React, { Component } from 'react';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class ProfilePicture extends Component {
    constructor(props) {
        super(props)
        // this.state = {
        //   name: this.props.name || 'Test'
        // }
    }


    render() {
        return (
            <View style={{ width: 130, height: 110, display: 'flex', alignItems: 'center' }}>
              {this.props.pictureType == 'filesystem' && (
                <Image source={this.props.picture} style={{ width: 110, height: 110, borderRadius: 5}} />
              )}
              {this.props.pictureType == 'web' && (
                <Image
                  source={{uri: this.props.picture}}
                  style={{ width: this.state.imageWidth, height: this.state.imageWidth, borderRadius: 5}}
                  defaultSource={require('./images/preloading.gif')}
                />
              )}
              <Text style={styles.texiInImage}>{this.props.name}</Text>
              <View style={styles.iconContainer}>
                {this.props.cameraIcon && (
                  <TouchableOpacity onPress={this.props.camerePress}>
                    <Image resizeMode="contain" source={require('./images/camera.png')} style={{ width: 25}} />
                  </TouchableOpacity>
                )}
                {this.props.numbers && (
                  <View style={styles.numberTextView}>
                    <Text style={styles.numberText}>+{this.props.numbers}</Text>
                  </View>
                )}
              </View>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    numberTextView: {
      backgroundColor: 'rgba(50, 50, 50, 0.5)',
      borderRadius: 5,
      padding: 3,
    },
    numberText: {
      color: 'white',
      backgroundColor: 'transparent',
      fontSize: 11,
    },
    iconContainer: {
      width: 110,
      display: 'flex',
      height: 40,
      backgroundColor: 'transparent',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 10,
      position: 'absolute',
      top: 0,
      left: 10,
      flexDirection:'row',
    },
    camera: {
        resizeMode:'contain',
        height: 25,
        position:'absolute',
        top:0,
        left:0
    },
    texiInImage: {
        width: 100,
        textAlign: 'center',
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',
        color: 'white',
        position: 'absolute',
        bottom: 10,
        left: 15,
        backgroundColor:'rgba(38,32,37,0.5)'
    },
    NumberInImage:{
        fontFamily: 'Montserrat-Regular',
        color: 'white',
        position: 'absolute',
        top: 3,
        right: 10,
        backgroundColor:'rgba(38,32,37,0.5)',
        borderRadius:5,
        paddingHorizontal:2
    },
});
