import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class popupsmall extends Component {
    static navigationOptions = {
        title: 'popupsmall',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>

                <View style={styles.popupContainer}>

                <Image source={require('./img/Layer 2.png')} style={{ position:'absolute', width:width, height:height, top:0,left:0 }} />

                    {/* HEADER */}
                    <View style={styles.header}>
                        <Text style={{ backgroundColor: 'transparent',fontSize: 17, fontFamily: 'Montserrat-Regular', color: 'white', textAlign: 'center' }}>AÑADIR PREFERENCIAS</Text>
                        <TouchableOpacity style={{ alignItems: 'center', position: 'absolute', right: -50, height: 20 }}
                          onPress={() => {
                            this.props.navigation.goBack()
                          }}
                        >
                            <Image source={require('./img/remove copy.png')} style={{ width: 20, height: 20, }} />
                        </TouchableOpacity>
                    </View>


                    {/* ADDRESS LOCATION */}
                    <View style={styles.addressLocation}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                                <Text style={{ backgroundColor: 'transparent',fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#627485',textAlign:'center' }}>Ubicación para la orden de servicio a domicilio:</Text>
                                <Text style={{ backgroundColor: 'transparent',fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#58a4b0',textAlign:'center'}}>Calle Durazno, 1 26383 Madrid</Text>
                            </View>
                            <TouchableOpacity style={{ alignItems: 'center', position: 'absolute', right: -4, height: 30 }} >
                                <Image source={require('./img/pen blue.png')} style={{ width: 30, height: 30, }} />
                            </TouchableOpacity>
                        </View>
                    </View>


                    {/* IMAGES*/}
                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column',marginTop:20 }}>
                        <Image source={require('./img/dash.png')} style={{ position:'absolute',top:-50}} />
                        <Image source={require('./img/Shape171.png')} style={{ width: 25, height: 25, }} />
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                        <Text style={{ backgroundColor: 'transparent',fontSize: 13, fontFamily: 'Montserrat-Regular', color: 'white', borderBottomColor: '#353e48', borderBottomWidth: 1, paddingBottom: 10, }}>mantener la información como plantilla?</Text>
                        <Text style={{ backgroundColor: 'transparent',fontSize: 13, fontFamily: 'Montserrat-Regular', color: '#e5eafa', }}>Cantidad de metros cuadrados de la propiedad:</Text>
                    </View>



                    {/* BRUSH CONTAINER*/}
                    <View style={styles.brushContainer}>
                        <View style={{ width: 45, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ backgroundColor: 'transparent',color: 'white' }}> 0 </Text>
                        </View>
                        <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
                            <View style={styles.icons}>
                                <Image source={require('./img/blue circle.png')} style={styles.blueCircle} />
                                <Image source={require('./img/cleaning.png')} style={styles.brush} />
                            </View>
                            <Text style={{ backgroundColor: 'transparent',textDecorationLine: 'underline', width: 100, fontSize: 14, fontFamily: 'Montserrat-Regular', color: 'white', borderBottomColor: '#353e48', textAlign: 'center', top: -10 }}>70 m</Text>
                            <Text style={{ backgroundColor: 'transparent',fontSize: 9, fontFamily: 'Montserrat-Regular', color: 'white', top: -30,right:-20 }}>2</Text>
                        </View>
                        <View style={{ width: 45, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ backgroundColor: 'transparent',color: 'white' }}> 150 </Text>
                        </View>
                    </View>


                    {/* DROPDOWN*/}
                    <View style={styles.dropdown}>

                        <TouchableOpacity>
                            <View style={{ width: 33, height: 24, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                            </View>
                        </TouchableOpacity>
                        <Text style={{ backgroundColor: 'transparent',color: 'white', fontFamily: 'Montserrat-Regular', fontSize: 12 }}>dormitorios & </Text>

                        <TouchableOpacity>
                            <View style={{ width: 33, height: 24, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                            </View>
                        </TouchableOpacity>
                        <Text style={{ backgroundColor: 'transparent',color: 'white', fontFamily: 'Montserrat-Regular', fontSize: 12 }}>baños en la propiedad </Text>

                    </View>


                    {/* TEXT INPUT*/}
                    <View style={styles.textInputSection}>
                        <Image source={require('./img/Ellipse 31 copy 8.png')} style={{ width: 60, height: 60, flex: 1, }} />
                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="white"
                            placeholder="Otros deseos"
                            placeholderTextColor="white"
                            style={{ width: 130, fontSize: 12, fontFamily: 'Montserrat-Regular', color: 'white', flex: 4, marginRight: 10, }}></TextInput>
                    </View>


                        <TouchableOpacity style={{marginTop:15}}>
                            <Text style={{ backgroundColor: 'transparent',color: 'white', fontSize: 20, fontFamily: 'Montserrat-Regular' }}>SALVAR</Text>
                        </TouchableOpacity>


                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 'auto',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'rgba(50, 50, 50, 0.4)',
    },
    popupContainer: {
        marginTop: 50,
        width: 320,
        height: 450,
        backgroundColor: '#3c4650',
        borderColor: '#58a4b0',
        borderWidth: 2,
        alignItems: 'center',
    },
    header: {
        marginTop:10,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    addressLocation: {
        display:'flex',
        width: width-55,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    icons: {
        width: 60,
        height: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',


    },
    brush: {
        width: 25,
        height: 25,
        position: 'absolute'
    },
    blueCircle: {
        width: 90,
        height: 90,

    },
    brushContainer: {
        width: width - 30,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#353e48'
    },
    sortdown: {
        width: 16,
        height: 11,
        position: 'absolute'
    },
    dropdown: {
        width: width - 5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20,
        marginTop:15
    },
    textInputSection: {
        width: width,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 20,
    }
});
