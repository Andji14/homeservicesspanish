import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import ProfilePicture from '../ProfilePicture/ProfilePicture';
import OrdersSlider from '../OrdersSlider/OrdersSlider';
import CalendarSlider from '../CalendarSlider/CalendarSlider';

import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';




var { height, width } = Dimensions.get('window');

export default class MyProfile extends Component {

    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>

                <Image source={require('./img/Layer 2.png')} style={styles.userImg} />

                <StatusBar
                    backgroundColor="#3fabbe"
                    barStyle="light-content"
                />

                {/* HEADER */}

                <Header icon="thumbs-up" navigationType="title" title="MI PERFIL" logout="logout" />



                {/* MAIN CONTAINER */}
                <View style={styles.mainContainer}>


                    {/* PROFILE INFO */}
                    <View style={{ height: 110, width: width, flexDirection: 'row', marginVertical: 5 }}>

                        <ProfilePicture
                          pictureType={'filesystem'}
                          picture={require('./img/Pic.png')}
                          name="Monica Kim"
                        />
                        {/* <ProfilePicture pictureType={'web'} picture={'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png'}/> */}

                        <View style={styles.topPresonalInfoContainer}>
                            <View>
                                <Image source={require('./img/e-mail.png')} style={{ width: 30, height: 30, }} />
                            </View>
                            <View>
                                <Image source={require('./img/location-pin.png')} style={{ width: 30, height: 30, }} />
                            </View>
                            <View>
                                <Image source={require('./img/phone-number.png')} style={{ width: 30, height: 30, }} />
                            </View>
                        </View>


                        <View style={styles.userInfo} >
                            <Text style={styles.userInfoCoordinates}>monica,kim@gmx.de</Text>
                            <Text style={styles.userInfoCoordinates}>Calle Aduana,2 MADRID SPAIN</Text>
                            <Text style={styles.userInfoCoordinates}>(001) 44 15 32 53</Text>
                        </View>

                        <View style={styles.topPencil} >
                            <TouchableOpacity onPress={() => {
                              this.props.navigation.navigate('MyBusinessProfile');
                            }}>
                                <Image source={require('./img/pencil-edit-button.png')} style={styles.topPencilImg} />
                            </TouchableOpacity>
                        </View>
                    </View>


                    {/* LINE */}
                    <View style={styles.line} />


                    {/* Payment settings */}
                    <View>
                        <View style={styles.paymentSettings}>
                            <Text style={styles.paymentSettingsText}>CONFIGURACIÓN DE PAGO</Text>
                            <TouchableOpacity>
                                <Image source={require('./img/copy.png')} style={{ width: 30, height: 30, }} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.paymentSectionContainer}>
                            <Image source={require('./img/card.png')} style={{ width: 30, height: 25, marginHorizontal: 15 }} />
                            <TextInput
                                autoCorrect={false}
                                underlineColorAndroid="transparent"
                                placeholder="**** **** ****2828 "
                                placeholderTextColor="#FFFFFF"
                                style={{ width: 150, textAlign: 'center', color: 'white', }}></TextInput>
                            <TouchableOpacity>
                                <Image source={require('./img/pencil-edit-button.png')} style={styles.cardIcons} />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./img/CLOSE-WHOLE-SCREEN.png')} style={styles.cardIcons} />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require('./img/Checked.png')} style={[styles.cardIcons, { width: 30 }]} />
                            </TouchableOpacity>
                        </View>

                    </View>


                    {/* LINE */}
                    <View style={styles.line} />


                    {/* PAST ORDERS */}
                    <View style={styles.pastOrdersContainer} >
                        <Text style={{backgroundColor: 'transparent', fontSize: 15, fontFamily: 'Montserrat-Light', color: 'white', marginRight: 10 }}>ÓRDENES ANTERIORES Y FACTURA</Text>
                        <TouchableOpacity>
                            <View style={styles.blueButton}>
                                <Text style={{ backgroundColor: 'transparent', color: 'white', fontWeight: 'bold' }}>...</Text>
                            </View>
                        </TouchableOpacity>
                    </View>


                    {/* MONTHS */}
                    <View style={{
                      width: width - 20,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'center',
                      height: 20
                    }}>
                      <CalendarSlider />
                    </View>


                    {/* ORDER INFO*/}
                    <OrdersSlider  leftColor='#0dbff2' shadowColor="#B00702"/>


                    {/* LINE */}
                    <View style={styles.line} />


                    {/* SHARE SECTION */}
                    <View style={styles.shareContainer}><TouchableOpacity style={{ flex: 2 }}>
                        <Image source={require('./img/share.png')} style={{ width: 30, height: 30, marginRight: 10 }} />
                    </TouchableOpacity>
                        <View style={{ flex: 10 }}>
                            <Text style={styles.shareText}>Comparte la aplicación para obtener un descuento en tu próximo pedido</Text>
                        </View>
                    </View>

                    {/* LINE */}
                    <View style={styles.line} />


                    {/*LANGUAGE SECTION */}
                    <View style={styles.languageContainer}>
                        <Image source={require('./img/translation.png')} style={{ width: 30, height: 30 }} />
                        <TouchableOpacity style={{justifyContent:'center',alignItems:'center',}}>
                            <Text style={{  backgroundColor: 'transparent',fontSize: 14, fontFamily: 'Montserrat-Light', color: 'white', marginHorizontal: 5 }}>IDIOMA</Text>
                        </TouchableOpacity>

                        <Image source={require('./img/info.png')} style={{ width: 30, height: 30 }} />
                        <TouchableOpacity style={{justifyContent:'center',alignItems:'center',}}>
                            <Text style={{  backgroundColor: 'transparent',fontSize: 14, fontFamily: 'Montserrat-Light', color: 'white', marginHorizontal: 5 }}>INFORMACIÓN</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                {/* FOOTER */}
                <Footer
                  icon={['user', 'home']}
                  hightlightedIcon="user"
                  navigationType="navigate"
                  homePress={() => {
                    this.props.navigation.navigate('FeedUpcoming');
                  }}
                  bellPress={() => {
                    this.props.navigation.navigate('Home');
                  }}
                />

            </View>

        );
    }
}




const styles = StyleSheet.create({
    footerBell: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 3,
        display: 'flex',
        borderColor: '#bf4e30',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 3,
        left: (width / 2) - 30,
    },
    languageContainer: {
        width: width - 50,
        height: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        marginTop:10
    },
    shareText: {
        fontSize: 14,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        backgroundColor: 'transparent',
    },
    shareContainer: {
        width: width - 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        marginVertical: 5,
    },
    clickForBill: {
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center'
    },
    orderMonthAndName: {
        width: 90,
        height: 80,
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center',
    },
    orderInfo: {
        width: 220,
        height: 75,
        borderLeftColor: '#0dbff2',
        borderRightColor: '#323c46',
        borderTopColor: '#323c46',
        borderBottomColor: '#323c46',
        borderWidth: 3,
        margin: 5,
        backgroundColor: '#445059'
    },
    monthTextStyle: {
        color: '#989ea3',
        marginRight: 2,
        textAlign: 'center',
        fontFamily: 'Montserrat-Light'
    },
    monthsContainer: {
        width: width,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
    },
    blueButton: {
        width: 35,
        height: 22,
        borderRadius: 15,
        backgroundColor: '#004ca0',
        justifyContent: 'center',
        alignItems: 'center',
    },
    pastOrdersContainer: {
        width: width - 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        marginVertical:10, 
    },
    paymentSettingsText: {
        fontSize: 15,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        marginRight: 5,
        backgroundColor: 'transparent',
    },
    paymentSettings: {
        width: width - 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        marginTop: 5
    },
    line: {
        width: width - 60,
        borderBottomColor: '#999ea3',
        borderBottomWidth: 1,
    },
    topPencilImg: {
        width: 30,
        height: 30,
        borderColor: '#999ea3',
        borderWidth: 1
    },
    topPencil: {
        width: 30,
        height: 110,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    userInfoCoordinates: {
        fontSize: 10,
        fontFamily: 'Montserrat-Light',
        color: 'white',
    },
    userInfo: {
        width: 120,
        height: 110,
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        justifyContent: 'space-around',
        paddingLeft: 5
    },
    topPresonalInfoContainer: {
        width: 35,
        height: 110,
        flexDirection: 'column',
        borderRightColor: '#02bff3',
        borderTopColor: '#323c46',
        borderLeftColor: '#323c46',
        borderBottomColor: '#323c46',
        borderWidth: 2,
        justifyContent: 'space-around',
        alignContent: 'center',

    },
    texiInImage: {
        width: 100,
        textAlign: 'center',
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',
        color: 'white',
        position: 'absolute',
        top: 70,
        left: 15
    },
    mainContainer: {
        height: 485,
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    myProfile: {
        fontSize: 18,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        fontWeight: 'bold'
    },
    userImg: {
        position: 'absolute',
        width: width,
        height: height,
        top: 0,
        left: 0
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#323c46',

    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10


    },
    footerContainer: {
        width: width,
        height: 45,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    cardIcons: {
        width: 25,
        height: 25,
        marginHorizontal: 2
    },
    paymentSectionContainer: {
        width: width - 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
