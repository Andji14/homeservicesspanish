import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';


var { height, width } = Dimensions.get('window');

export default class BusinessOrderReview extends Component {
    static navigationOptions = {
        title: 'FeedUpcoming',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>
                <Header moreOption={true}
                onPressFeatured={() => {
                  this.props.navigation.navigate('FeedFeatured');
                }}
                onPressUpcoming={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                pressMenu={() => {
                  this.props.navigation.navigate('NotificationScr');
                }}
                />
                {/* MAIN CONTAINER */}
                <View style={{ width: width - 20, height: height, backgroundColor: 'white', alignItems: 'center', flex: 1, marginTop: 5, }}>

                    <View style={{ width: width - 20, height: 80, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', }}>

                        {/* IMAGE CONTAINER */}
                        <View style={{ width: 60, height: 80, }}>
                            <View style={{ height: 80, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('./img/profile.png')} style={{ width: 50, height: 50, }} />
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>120 km de distancia</Text>
                            </View>
                        </View>


                        {/* TEXT CONTAINER */}
                        <View style={{ width: width - 85, height: 80, justifyContent: 'flex-end',marginLeft:5 }}>
                            <View>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'black', backgroundColor: 'transparent' }}>Monika Kim (exped: 19 Oct 2017 17:10) </Text>
                            </View>

                            <View style={{ height: 60, backgroundColor: '#9d9db4', borderRadius: 10, }}>
                                <View style={{ height: 20, flexDirection: 'row', }}>
                                    <View style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('./img/tabbar_icon_location.png')} style={{ width: 50, height: 50, }} />
                                    </View>
                                    <Text style={{ fontSize: 13, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'right', backgroundColor: 'transparent' }}>Rosas 2, 28003 Madrid Spain($190)</Text>
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', backgroundColor: 'transparent' }}>Problema de fontanería que causa un grave desperdicio de agua.Lorem ispum dolor </Text>
                            </View>
                        </View>
                    </View>


                    {/* IMG CONTAINER */}
                    <View style={{ width: width - 40, height: 120, marginTop: 10, alignItems: 'center', borderRadius: 10 }}>
                        <Image source={require('./img/plumbing-issue.png')} style={{ width: width - 40, height: 120, borderRadius: 10 }} />
                    </View>



                    {/* BUTTONS */}
                    <View style={{
                        marginTop: 10,
                        width: width - 40,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        justifyContent: 'space-between',
                        alignContent: 'center',

                    }}>

                        <View >
                            <TouchableOpacity onPress={() => {
                              this.props.navigation.navigate('BusinessAcceptDetails');
                            }} style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#006400', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                                <Image source={require('./img/accept.png')} style={{ width: 25, height: 25, }} />
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5, backgroundColor: 'transparent' }}>ACEPTAR</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#ff0000', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                                <Image source={require('./img/decline.png')} style={{ width: 25, height: 25, }} />
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5, backgroundColor: 'transparent' }}>DECLINAR</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#ffa500', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                                <Image source={require('./img/snooze-icon.png')} style={{ width: 25, height: 25, }} />
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5, backgroundColor: 'transparent' }}>PENDIENTE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{ marginVertical: 5, }}>
                        <Image source={require('./img/current copy 2.png')} style={{ width: width - 40, height: 4, }} />
                    </View>



                    {/* OTHER OFFERS */}
                    <View >
                        <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Regular', textAlign: 'center', color: 'black', marginVertical: 10, backgroundColor: 'transparent' }}>OTRAS OFERTAS</Text>
                    </View>


                    <View style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: width,
                        flexDirection: 'row',
                        marginTop: 5,
                    }} >
                        <View style={{ width: (width - 20) / 4 + 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>EMPRESA</Text>
                        </View>
                        <View style={{ width: (width - 20) / 4 + 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>CLASIFICACIÓN</Text>
                        </View>
                        <View style={{ width: (width - 20) / 4 - 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>PRECIO</Text>
                        </View>
                        <View style={{ width: (width - 20) / 4 - 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'right', color: 'black', backgroundColor: 'transparent' }}>REACCIÓN</Text>
                        </View>
                    </View>



                    {/* LINE */}
                    <View style={styles.line} />


                    {/* FOUR SECTIONS FIRST */}
                    <View style={styles.fourSections}>

                        <View style={[styles.fourSectionsBigContainer, { flexDirection: 'row' }]} >
                            <Image source={require('./img/profile.png')} style={{ width: 30, height: 35, flex: 3 }} />
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', color: 'black', backgroundColor: 'transparent', flexWrap: 'wrap', flex: 5 }}>Marble Arch  Repairs Ltd.</Text>
                        </View>

                        <View style={styles.fourSectionsBigContainer}>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907 copy 4.png')} style={{ width: 15, height: 15, }} />
                                </View>
                            </View>
                            <View>
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>(129)</Text>
                            </View>
                        </View>

                        <View style={styles.fourSectionsSmallContainer}>
                            <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>EUR149</Text>
                        </View>
                        <View style={styles.fourSectionsSmallContainer}>
                            <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>20.10.17  @17:00h</Text>
                        </View>
                    </View>




                    {/* FOUR SECTIONS SECOND */}
                    <View style={styles.fourSections}>

                        <View style={[styles.fourSectionsBigContainer, { flexDirection: 'row' }]} >
                            <Image source={require('./img/profile.png')} style={{ width: 30, height: 35, flex: 3 }} />
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', color: 'black', backgroundColor: 'transparent', flex: 5 }}>Marble Arch  Repairs Ltd.</Text>
                        </View>

                        <View style={styles.fourSectionsBigContainer}>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907 copy 4.png')} style={{ width: 15, height: 15, }} />
                                </View>
                            </View>
                            <View>
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>(129)</Text>
                            </View>
                        </View>

                        <View style={styles.fourSectionsSmallContainer}>
                            <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>EUR149</Text>
                        </View>
                        <View style={styles.fourSectionsSmallContainer}>
                            <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>20.10.17  @17:00h</Text>
                        </View>
                    </View>
                </View >


                {/* FOOTER */}
                <Footer icon={['user', 'home']} hightlightedIcon="home" navigationType="navigate"
                userPress={() => {
                  this.props.navigation.navigate('MyProfile');
                }}
                homePress={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                />
            </View >
        );
    }
}

const styles = StyleSheet.create({
    line: {
        width: width - 40,
        borderBottomColor: '#999ea3',
        borderBottomWidth: 1,
    },
    fourSectionsSmallContainer: {
        width: (width - 20) / 4 - 20,
        height: 50,
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSectionsBigContainer: {
        width: (width - 20) / 4 + 20,
        height: 50,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSections: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: 50,
        flexDirection: 'row',
        marginTop: 5,
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 470
    },
    locationContainer: {
        width: 200,
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(44,57,66,0.5)',
        left: -60
    },
    location: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
});
