import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class OfferReview extends Component {
    static navigationOptions = {
        title: 'OfferReview',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>
                {/* HEADER */}
                <Header icon="back" navigationType="navigate"
                onPressBack={() => {
                  this.props.navigation.goBack();
                }}
                onPressFeatured={() => {
                  this.props.navigation.navigate('FeedFeatured');
                }}
                onPressUpcoming={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                />



                {/* MAIN CONTAINER */}
                <View style={{ width: width, height: 480, backgroundColor: 'white', alignItems: 'center', flex: 1, marginTop: 5 }}>

                    <View style={{ justifyContent: 'center', alignItems: 'center', height: 80, width: width - 20, flexDirection: 'row' }}>

                        {/* IMAGE CONTAINER */}
                        <View style={{ width: 60, height: 80, }}>
                            <View style={{ height: 80, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('./img/profile.png')} style={{ width: 50, height: 50, }} />
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center' }}>120 km de distancia</Text>
                            </View>
                        </View>


                        {/* TEXT CONTAINER */}
                        <View style={{ width: width - 85, height: 80, justifyContent: 'flex-end',marginLeft:5 }}>
                            <View>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'black' }}>Monika Kim (exped: 19 Oct 2017 17:10) </Text>
                            </View>

                            <View style={{ backgroundColor: '#9d9db4', borderRadius: 10, height: 60 }}>
                                <View style={{ height: 20, flexDirection: 'row', }}>
                                    <TouchableOpacity style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('./img/tabbar_icon_location.png')} style={{ width: 50, height: 50, }} />
                                    </TouchableOpacity>
                                    <Text style={{ fontSize: 13, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'right', }}>Rosas 32, 28003 Madrid Spain($190)</Text>
                                </View>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', }}>Problema de fontanería que causa un grave desperdicio de agua.Lorem ispum dolor </Text>
                            </View>
                        </View>
                    </View>


                    {/* IMG CONTAINER */}
                    <View style={{ marginTop: 10, width: width - 40, height: 120, alignItems: 'center', borderRadius: 10 }}>
                        <Image source={require('./img/plumbing-issue.png')} style={{ width: width - 40, height: 120, borderRadius: 10 }} />
                    </View>

                    <View style={{ marginVertical: 5, }}>
                        <Image source={require('./img/current copy 2.png')} style={{ width: width - 40, height: 4, }} />
                    </View>



                    {/* PLACED OFFERS */}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: width - 40, marginBottom: 5 }}>
                        <TouchableOpacity>
                            <Image source={require('./img/slider-arrow.png')} style={{ width: 15, height: 15, }} />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Regular', textAlign: 'center', color: 'black' }}>OFERTAS COLOCADAS</Text>
                        <TouchableOpacity>
                            <Image source={require('./img/slider-arrow-left.png')} style={{ width: 15, height: 15, }} />
                        </TouchableOpacity>
                    </View>




                    <View style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: width,
                        flexDirection: 'row',
                        marginTop: 5,
                    }} >
                        <View style={{ width: (width - 20) / 4 + 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>EMPRESA</Text>
                        </View>
                        <View style={{ width: (width - 20) / 4 + 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>CLASIFICACIÓN</Text>
                        </View>
                        <View style={{ width: (width - 20) / 4 - 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>PRECIO</Text>
                        </View>
                        <View style={{ width: (width - 20) / 4 - 20, }}>
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'right', color: 'black', backgroundColor: 'transparent' }}>REACCIÓN</Text>
                        </View>
                    </View>



                    {/* LINE */}
                    <View style={styles.line} />




                    {/* FOUR SECTIONS FIRST */}
                    <View style={styles.fourSections}>

                        <View style={[styles.fourSectionsBigContainer, { flexDirection: 'row' }]} >
                            <Image source={require('./img/profile.png')} style={{ width: 30, height: 35, flex: 3 }} />
                            <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', color: 'black', backgroundColor: 'transparent', flexWrap: 'wrap', flex: 5 }}>Marble Arch  Repairs Ltd.</Text>
                        </View>

                        <View style={styles.fourSectionsBigContainer}>
                            <View style={{ flexDirection: 'row' }}>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                </View>
                                <View>
                                    <Image source={require('./img/Shape 907 copy 4.png')} style={{ width: 15, height: 15, }} />
                                </View>
                            </View>
                            <View>
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>(129)</Text>
                            </View>
                        </View>

                        <View style={styles.fourSectionsSmallContainer}>
                            <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>EUR149</Text>
                        </View>
                        <View style={styles.fourSectionsSmallContainer}>
                            <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>20.10.17  @17:00h</Text>
                        </View>
                    </View>

                    {/* TEXT CONTAINER */}

                    <View style={{ width: width, justifyContent: 'flex-end', flexDirection: 'row', }}>

                        <View style={{ backgroundColor: '#9d9db4', borderRadius: 10, width: width - 80, marginHorizontal: 10, justifyContent: 'flex-end' }}>
                            <ScrollView>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', marginLeft: 8 }}>Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, dolor Lorem ipsum dolor sit amet, adipiscing elit</Text>
                            </ScrollView>
                        </View>
                    </View>

                    {/* BUTTONS */}
                    <View style={{
                        marginTop: 10,
                        width: width - 40,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        justifyContent: 'space-between',
                        alignContent: 'center',

                    }}>

                        <View >
                            <TouchableOpacity onPress={() => {
                              this.props.navigation.navigate('FeedUpcoming');
                            }} style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#006400', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                                <Image source={require('./img/accept.png')} style={{ width: 30, height: 30, }} />
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5 }}>ACEPTAR</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#ff0000', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                                <Image source={require('./img/decline.png')} style={{ width: 30, height: 30, }} />
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5 }}>DECLINAR</Text>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <TouchableOpacity style={{ width: ((width - 40) / 3) - 10, height: 35, borderRadius: 5, backgroundColor: '#ffa500', justifyContent: 'center', flexDirection: 'row', alignItems: 'center', borderColor: 'black', borderWidth: 1 }}>
                                <Image source={require('./img/snooze-icon.png')} style={{ width: 30, height: 30, }} />
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Bold', color: 'white', textAlign: 'center', marginRight: 5 }}>PENDIENTE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                {/* FOOTER */}
                <Footer icon={['user', 'home']} hightlightedIcon="home" navigationType="navigate"
                userPress={() => {
                  this.props.navigation.navigate('MyProfile');
                }}
                homePress={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                bellPress={() => {
                  this.props.navigation.navigate('BusinessOrderReview');
                }}
                />
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 470
    },
    locationContainer: {
        width: 200,
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(44,57,66,0.5)',
        left: -60
    },
    location: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    fourSectionsSmallContainer: {
        width: (width - 20) / 4 - 20,
        height: 50,
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSectionsBigContainer: {
        width: (width - 20) / 4 + 20,
        height: 50,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSections: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: 50,
        flexDirection: 'row',
        marginTop: 5,
    },
    line: {
        width: width - 40,
        borderBottomColor: '#999ea3',
        borderBottomWidth: 1,
    },
});
