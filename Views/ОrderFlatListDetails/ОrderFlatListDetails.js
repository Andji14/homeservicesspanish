import React, { Component } from 'react';
import Dimensions from 'Dimensions';

import {
    View,
    FlatList,
    Text,
    Image,
    StyleSheet
} from 'react-native';

var moment = require('moment');
var { height, width } = Dimensions.get('window');

export default class OrderFlatListDetails extends Component {


    render() {
        return (
            <View style={styles.container}>

                <View style={styles.orderInfo} onLayout={(event) => {
                    this.setState({ shadowHeight: event.nativeEvent.layout.height })
                }}>

                    {this.props.shadow == 'shadow' && (
                        <View style={[styles.shadow, {
                            height: this.state.shadowHeight,
                            backgroundColor: this.props.leftColor,
                            shadowColor: this.props.shadowColor,
                        }]}></View>
                    )}


                    <View style={{ flexDirection: 'row', height: 45 }}>
                        {this.props.dataWithService == 'dataWithService' && (
                            <Text style={styles.orderMonthAndName}>{item.date.format('MMM Do')} {item.serviceType}</Text>
                        )}
                        {this.props.image == 'image' && (
                            <View style={{ width: 35, height: 35, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={item.avatar} style={{ width: 60, height: 60, }} />
                            </View>
                        )}
                        {this.props.company == 'company' && (
                            <Text style={styles.orderMonthAndName}>{item.companyName}</Text>
                        )}
                    </View>


                    {this.props.button == 'button' && (
                        <View style={{ height: 20 }}>
                            <Text style={styles.clickForBill}>{item.button}</Text>
                        </View>
                    )}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginTop: 10,
        width: width,
        paddingHorizontal: 5,
    },
    shadow: {
        width: 4,
        height: 10,
        backgroundColor: 'red',
        position: 'absolute',
        top: 0,
        left: 0,
        shadowOffset: { width: -3, height: 0 },
        shadowOpacity: 0.4,
        shadowRadius: 4,
    },
    orderInfo: {
        width: 220,
        height: 75,
        margin: 5,
        backgroundColor: 'rgba(72,80,89, 0.5)',
    },
    clickForBill: {
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
    orderMonthAndName: {
        width: 90,
        height: 80,
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center',
        backgroundColor: 'transparent'
    },
});
