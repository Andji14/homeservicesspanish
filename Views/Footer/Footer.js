import React, { Component } from 'react';
import {
    Platform,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class Footer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            icon: this.props.icon,
            navigationType: this.props.navigationType,
        }
    }


    render() {

        return (
            <View>
                <View style={styles.footerContainer} >

                    {/* HOME ICON */}
                    {this.state.icon.indexOf('home') != -1 && (
                        <View style={{ flexDirection: 'column', width: 40, alignItems: 'center' }}>
                            <TouchableOpacity onPress={this.props.homePress}>
                                <Image source={require('./img/house-black-silhouette-without-door.png')} style={{ width: 35, height: 35, }} />
                            </TouchableOpacity>
                            {this.props.hightlightedIcon == 'home' && (
                              <Image source={require('./img/current copy 3.png')} style={{ width: 58, height: 15, position: 'absolute', top: 23 }} />
                            )}
                        </View>
                    )}

                    {/* USER ICON */}
                    {this.state.icon.indexOf('user') != -1 && (
                        <View style={{ flexDirection: 'column', width: 40, alignItems: 'center' }}>
                            <TouchableOpacity onPress={this.props.userPress}>
                                <Image source={require('./img/user-avatar.png')} style={{ width: 35, height: 35, }} />
                            </TouchableOpacity>
                            {this.props.hightlightedIcon == 'user' && (
                              <Image source={require('./img/current copy 3.png')} style={{ width: 58, height: 15, position: 'absolute', top: 23 }} />
                            )}
                        </View>
                    )}

                </View>


                {/* BELL CIRCLE */}
                {this.state.navigationType == 'navigate' && (
                    <View style={styles.footerBell}>
                        <TouchableOpacity onPress={this.props.bellPress}>
                            <Image source={require('./img/bell.png')} style={{ width: 40, height: 40, }} />
                        </TouchableOpacity>
                    </View>
                )}
                {this.state.navigationType == 'text' && (
                    <View style={styles.footerBell}>
                        <TouchableOpacity onPress={this.props.onPressText}>
                            <Text style={{ fontSize: 14,fontFamily: 'Montserrat-Regular', fontWeight: 'bold' }}>{this.props.title.toUpperCase()}</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </View>
        );
    }
}


const styles = StyleSheet.create({

    footerBell: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 3,
        display: 'flex',
        borderColor: '#bf4e30',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 3,
        left: (width / 2) - 30,
    },
    footerContainer: {
        width: width,
        height: 45,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
});
