import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,
    KeyboardAvoidingView
} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class BusinessAcceptDetails extends Component {
    static navigationOptions = {
        title: 'BusinessAcceptDetails',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>

                <View style={styles.popupContainer}>

                    <Image source={require('./img/Layer 2.png')} style={{ position: 'absolute', width: width, height: height, top: 0, left: 0 }} />

                    {/* HEADER */}
                    <View style={styles.header}>
                        <Text style={{fontSize: 15, fontFamily: 'Montserrat-Regular', color: 'white'}}>AGREGAR DETALLES PARA ACEPTAR</Text>
                        <TouchableOpacity onPress={() => {
                          this.props.navigation.goBack();
                        }} style={{ alignItems: 'center', height: 20 }}>
                            <Image source={require('./img/remove copy.png')} style={{ width: 20, height: 20, }} />
                        </TouchableOpacity>
                    </View>


                    {/* ADDRESS LOCATION */}
                    <View style={styles.addressLocation}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                                <Text style={{textAlign:'center', backgroundColor: 'transparent', fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#627485' }}>Ubicación para la orden de servicio a domicilio:</Text>
                                <Text style={{ textAlign:'center',backgroundColor: 'transparent', fontSize: 14, fontFamily: 'Montserrat-Regular', color: '#58a4b0' }}>Rosas 32, 28003 Madrid Spain</Text>
                            </View>
                        </View>
                    </View>


                    {/* IMAGES*/}
                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                        <Image source={require('./img/dash.png')} style={{ width: 90, height: 30, }} />
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                        <Text style={{ backgroundColor: 'transparent', fontSize: 13, fontFamily: 'Montserrat-Regular', color: '#e5eafa', }}>SU PRECIO PARA LA ORDEN DE SERVICIO:</Text>
                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="white"
                            placeholder="Agregue su precio aquí"
                            placeholderTextColor="#e5eafa"
                            style={{ width: 130, fontSize: 12, fontFamily: 'Montserrat-Regular', color: 'white', }}></TextInput>

                        {/* LINE */}
                        <View
                            style={{
                                width: width - 80,
                                borderBottomColor: '#4a545d',
                                borderBottomWidth: 1,
                            }}
                        />
                        <Text style={{ backgroundColor: 'transparent', fontSize: 11, fontFamily: 'Montserrat-Regular', color: '#e5eafa', marginTop: 10 }}>ELIJA UNA FECHA Y HORA PARA LA REACCIÓN</Text>

                    </View>



                    {/*DROPDOWN CONTAINER*/}
                    <View style={styles.brushContainer}>
                        <View style={{ width: 120, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <Text style={{ color: '#e5eafa', fontFamily: 'Montserrat-Light' }}> 01.01.2017 </Text>
                            <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                        </View>

                        <View style={{ width: 120, height: 30, borderRadius: 16, backgroundColor: '#252c33', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                            <Text style={{ backgroundColor: 'transparent', color: '#e5eafa', fontFamily: 'Montserrat-Light' }}> 00:00 H </Text>
                            <Image source={require('./img/sort-down.png')} style={styles.sortdown} />
                        </View>
                    </View>

                    {/* LINE */}
                    <View
                        style={{
                            width: width - 80,
                            borderBottomColor: '#4a545d',
                            borderBottomWidth: 1,
                        }}
                    />




                    {/* TEXT INPUT*/}

                    <View style={styles.textInputSection}>
                        <Image source={require('./img/Ellipse 31 copy 8.png')} style={{ width: 60, height: 60, flex: 1, }} />

                        <TextInput
                            autoCorrect={false}
                            underlineColorAndroid="white"
                            placeholder="Agregar comentarios/preguntas adicionales "
                            placeholderTextColor="#e5eafa"
                            style={{ width: 130, fontSize: 12, fontFamily: 'Montserrat-Regular', color: 'white', flex: 8, marginRight: 10, }}></TextInput>

                    </View>


                    <TouchableOpacity style={{ marginTop: 30 }} onPress={() => {
                      this.props.navigation.goBack();
                    }}>
                        <Text style={{ backgroundColor: 'transparent', color: 'white', fontSize: 20, fontFamily: 'Montserrat-Regular' }}>SALVAR</Text>
                    </TouchableOpacity>


                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 'auto',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'rgba(50, 50, 50, 0.5)',
    },
    popupContainer: {
        marginTop: 50,
        width: 320,
        height: 450,
        backgroundColor: '#3c4650',
        borderColor: '#58a4b0',
        borderWidth: 2,
        alignItems: 'center',
    },
    header: {
        height: 30,
        width:width-50,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 10,   
    },
    addressLocation: {
        display:'flex',
        width: width-55,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',


    },
    icons: {
        width: 60,
        height: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',


    },
    brush: {
        width: 25,
        height: 25,
        position: 'absolute'
    },
    blueCircle: {
        width: 90,
        height: 90,

    },
    brushContainer: {
        width: width - 30,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        justifyContent: 'space-around',
        margin: 20,


    },
    sortdown: {
        width: 18,
        height: 13,
    },
    dropdown: {
        width: width - 5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 20,
        marginTop: 15
    },
    textInputSection: {
        width: width,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 20,
    }
});
