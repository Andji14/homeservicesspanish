import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import ServiceType from '../ServiceType/ServiceType';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,
    KeyboardAvoidingView,
    FlatList
} from 'react-native';
import Dimensions from 'Dimensions';


var { height, width } = Dimensions.get('window');

export default class Home extends Component {
    static navigationOptions = {
        title: 'Home',
    }
    constructor(props) {
        super(props)
        this.state = {
            selectedCategory: 'limpieza',

            categories: [
                {
                    image: 'limpieza',
                    service: 'limpieza',
                },
                {
                    image: 'electrician',
                    service: 'servicios de electricista',
                },
                {
                    image: 'painter-roller',
                    service: 'pintura para pared',
                },
                {
                    image: 'question-mark',
                    service: 'otros (especificar)',
                },
            ]
        }
    }
    render() {

        return (
            <View style={styles.container}>

                <StatusBar
                    backgroundColor="#3fabbe"
                    barStyle="light-content"
                />

                {/* HEADER */}
                <Header
                  icon="none"
                  navigationType="title"
                  title="PRESENTAR NUEVO ORDEN DE INICIO"
                  close="close"
                  titleStyles={{color: 'black', textDecorationLine:'underline'}}
                  onClose={() => {
                    this.props.navigation.goBack();
                  }}
                />



                {/* MAIN CONTAINER */}
                <ScrollView contentContainerStyles={{
                  height: height-110,
                  width: width - 10,
                  backgroundColor: 'white',
                }}>
                    <View style={{
                        width: width - 10,
                        height: height-110,
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: 'white',
                        marginVertical: 12,
                        borderRadius: 5,

                    }}>
                        {/* ADD PHOTOS */}
                          <View style={styles.addPhotos}>
                              <Text style={styles.topText} >AÑADE PHOTOS</Text>
                              <Text style={styles.leftText}>1</Text>
                              <View style={styles.topImgCont}>
                                  <Image source={require('./img/topImage.png')} style={styles.topImage} />
                                  <TouchableOpacity style={styles.camButt}>
                                      <Image source={require('./img/add-photos-bttn.png')} style={{ width: 30, height: 25 }} />
                                  </TouchableOpacity>
                                  <TouchableOpacity style={styles.remove}>
                                      <Image source={require('./img/remove.png')} style={{ width: 20, height: 20 }} />
                                  </TouchableOpacity>
                              </View>
                          </View>

                        {/* ASSIGN TO */}
                        <View style={styles.assign}>
                        </View>

                        {/* ICON CONTAINER */}
                        <View style={styles.iconsSectionContainer}>
                          <Text style={styles.topText} >UNA CATEGORÍA + PREFERENCIAS</Text>
                          <Text style={styles.leftText}>2</Text>
                          <FlatList
                            horizontal={true}
                            data={this.state.categories}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({item}) => {
                              if (item.image == this.state.selectedCategory) {
                                  console.log(item);
                                  return (
                                      <ServiceType
                                          color="orange"
                                          onPress={() => {
                                              this.setState({ selectedCategory: item.image, categories: this.state.categories.slice(0) });
                                          }}
                                          option={item.image == 'limpieza'}
                                          image={item.image}
                                          pressOption={() => {
                                            this.props.navigation.navigate('popupsmall');
                                          }}
                                          textTypeService={item.service}
                                          bold={true}
                                          icon="none"
                                      />
                                  );
                              }
                              return (
                                  <ServiceType color="black" onPress={() => {
                                      this.setState({ selectedCategory: item.image, categories: this.state.categories.slice(0) });
                                  }} image={item.image} textTypeService={item.service} />
                              );

                            }}
                          />

                        </View>


                        {/*LOCATION  */}

                        <View style={styles.mapSectionContainer}>
                            <Text style={styles.topText} >UBICACIÓN</Text>
                            <Text style={styles.leftText}>3</Text>
                            <View style={{
                              marginHorizontal: 20,
                              marginVertical: 5,
                              display: 'flex',
                              shadowOffset:{  width: 1,  height: 1,  },
                              shadowColor: 'black',
                              shadowOpacity: 0.5, }}
                            >
                                <Image source={require('./img/map.png')} style={{ width: width - 60, height: 60, borderRadius: 5 }} />
                            </View>
                            <Text style={{ backgroundColor: 'transparent',fontSize: 12, fontFamily: 'Montserrat-Light', left: 25, top: 60, position: 'absolute', color: 'black' }}>Calle Aduana,2 MADRID SPAIN</Text>
                            <Image source={require('./img/map_pin.png')} style={{ width: 50, height: 50, right: 100, top: 20, position: 'absolute' }} />
                        </View>
                        <View style={styles.paymentSectionContainer}>
                            <Text style={styles.topText} >PAGO</Text>
                            <Text style={styles.leftText}>4</Text>
                            <View style={styles.paymentContainer}>
                                <Image source={require('./img/card.png')} style={{ width: 30, height: 25, marginHorizontal: 15 }} />
                                <TextInput
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder="**** **** ****2828 "
                                    placeholderTextColor="black"
                                    style={{ width: 100, textAlign: 'center', color: 'white', fontSize: 10}}></TextInput>
                                <TouchableOpacity>
                                    <Image source={require('./img/pencil-edit-button.png')} style={{ width: 25, height: 25, marginHorizontal: 2 }} />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image source={require('./img/plus.png')} style={{ width: 25, height: 25, marginHorizontal: 2 }} />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Image source={require('./img/checkedblack.png')} style={{ width: 20, height: 20, marginHorizontal: 2 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>



                {/* FOOTER */}
                <Footer icon={['user', 'home']} navigationType="text" title="orden"
                  homePress={() => {
                    this.props.navigation.navigate('FeedUpcoming');
                  }}
                  userPress={() => {
                    this.props.navigation.navigate('MyProfile');
                  }}
                  onPressText={() => {
                    this.props.navigation.navigate('OfferReview');
                  }}
                />

            </View>


        );
    }
}

const styles = StyleSheet.create({
    paymentContainer: {
      width: width - 70,
      marginHorizontal: 20,
      marginVertical: 10,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: 'white',
      borderRadius: 5,
      shadowOffset:{  width: 1,  height: 1,  },
      shadowColor: 'black',
      shadowOpacity: 0.5,
    },
    leftText: {
      fontSize: 14,
      fontFamily: 'Montserrat-Light',
      color: 'black',
      position: 'absolute',
      top: 20,
      left: -10,
      backgroundColor: 'white',
      paddingHorizontal: 5,
      borderRadius: 4,
      borderWidth: 1,
      borderColor: 'black',
      zIndex: 10,
    },
    topText:{
      fontSize: 14,
      fontFamily: 'Montserrat-Light',
      color: 'black',
      position: 'absolute',
      top: -10,
      backgroundColor: 'white',
      paddingHorizontal: 5,
    },
    iconsSectionContainer: {
        width: width - 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor:'black',
        paddingVertical: 20,
        top: -1,
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    header: {
        width: width,
        height: 50,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        flexDirection: 'row',
    },

    addPhotos: {
        width: width - 40,
        height: 180,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'black',
        borderWidth: 1,
    },
    topImgCont: {
        width: width,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    topImage: {
        width: width - 70,
        height: 150,
        borderRadius: 5
    },
    camButt: {
        position: 'absolute',
        width: 40,
        height: 30,
        top: 110,
        right: 30
    },
    remove: {
        position: 'absolute',
        width: 20,
        height: 20,
        marginLeft: 10,
        top: 10,
        right: 40
    },

    icons: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: '#dfaa00',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        backgroundColor: '#0c1b54'
    },
    iconsNotBold: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: 'black',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
    brush: {
        width: 25,
        height: 25,
    },
    iconsImg: {
        width: 30,
        height: 35,
    },
    mapSectionContainer: {
        width: width - 40,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        top: -2,
    },
    paymentSectionContainer: {
        width: width - 40,
        height: 50,
        justifyContent: 'center',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        marginBottom: 10,
        top: - 3,
    },

});
