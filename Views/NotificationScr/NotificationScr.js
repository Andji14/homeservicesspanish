import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';
import Notification from '../Notification/Notification';


var { height, width } = Dimensions.get('window');

export default class NotificationScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            notifications: [
                {
                    key: 1,
                    datetime: new Date(),
                    message: 'Has recibido un mensaje nuevo',
                    color: '#ffe801',
                    type: 'reply',
                },
                {
                    key: 2,
                    datetime: new Date(),
                    message: 'Nueva solicitud de servicio cerca de su casa',
                    color: '#ffe801',
                    type: 'reply',
                },
                {
                    key: 3,
                    datetime: new Date(),
                    message: 'Su oferta ha sido aceptada',
                    color: '#ffe801',
                    type: 'reply',
                },
                {
                    key: 4,
                    datetime: new Date(),
                    message: 'Una nueva oferta para su solicitud de servicio a domicilio',
                    color: '#ffe801',
                    type: 'reply',
                },
                {
                    key: 5,
                    datetime: new Date(),
                    message: 'La empresa ABC solicita el pago',
                    color: '#ffe801',
                    type: 'reply',
                }
            ]
        }
    }
    render() {

        return (
            <View style={styles.container}>
                <Image source={require('./img/Layer 2.png')} style={{ position: 'absolute', width: width, height: height, top: 0, left: 0 }} />

                {/* HEADER */}
                <Header icon="notification"
                onPressFeatured={() => {
                  this.props.navigation.navigate('FeedFeatured');
                }}
                onPressUpcoming={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                />

                {/* MAIN CONTAINER */}
                <ScrollView contentContainerStyle={{
                    width: width,
                    height: height - 115,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                }}>
                    {
                        this.state.notifications.map((notif) => {
                            return (
                                <Notification
                                    datetime={notif.datetime}
                                    message={notif.message}
                                    color={notif.color}
                                    type={notif.type}
                                />
                            )
                        })
                    }
                </ScrollView>

                {/* FOOTER */}
                <Footer icon={['user', 'home']}  navigationType="navigate"
                userPress={() => {
                  this.props.navigation.navigate('MyProfile');
                }}
                homePress={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                bellPress={() => {
                  this.props.navigation.navigate('Home');
                }}
                />
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#323c46',
    },
});
