import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,

} from 'react-native';
import Dimensions from 'Dimensions';

var { height, width } = Dimensions.get('window');

export default class OrderSummary extends Component {
    static navigationOptions = {
        title: 'OrderSummary',
    }
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <View style={styles.container}>
                <Header icon="back" navigationType="navigate"
                onPressBack={() => {
                  this.props.navigation.goBack();
                }}
                onPressFeatured={() => {
                  this.props.navigation.navigate('FeedFeatured');
                }}
                onPressUpcoming={() => {
                  this.props.navigation.navigate('FeedUpcoming');
                }}
                />


                {/* MAIN CONTAINER */}
                <ScrollView>

                    <View style={{ width: width, height: height - 35, backgroundColor: 'white', alignItems: 'center', display: 'flex', marginTop: 5 }}>


                        <View style={{ justifyContent: 'center', alignItems: 'center', height: 80, width: width - 20, flexDirection: 'row' }}>

                            {/* IMAGE CONTAINER */}
                            <View style={{ width: 60, height: 80, }}>
                                <View style={{ height: 80, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image source={require('./img/profile.png')} style={{ width: 50, height: 50, }} />
                                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center' }}>120 km de distancia</Text>
                                </View>
                            </View>


                            {/* TEXT CONTAINER */}
                            <View style={{ width: width - 85, height: 80, justifyContent: 'flex-end',marginLeft:5 }}>
                                <View>
                                    <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'black' }}>Monika Kim (exped: 19 Oct 2017 17:10) </Text>
                                </View>

                                <View style={{ backgroundColor: '#9d9db4', borderRadius: 10, height: 60 }}>
                                    <View style={{ height: 20, flexDirection: 'row', }}>
                                        <TouchableOpacity style={{ width: 20, height: 20, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image source={require('./img/tabbar_icon_location.png')} style={{ width: 50, height: 50, }} />
                                        </TouchableOpacity>
                                        <Text style={{ fontSize: 13, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'right', }}>Rosas 32, 28003 Madrid Spain($190)</Text>
                                    </View>
                                    <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', }}>Problema de fontanería que causa un grave desperdicio de agua.Lorem ispum dolor </Text>
                                </View>
                            </View>
                        </View>

                        {/* RED LINE */}
                        <View style={{ marginVertical: 3, width: width - 10 }}>
                            <Image source={require('./img/current copy 2.png')} style={{ width: width - 10, height: 4, }} />
                        </View>



                        <View style={{ flexDirection: 'row', width: width - 20, justifyContent: 'center', alignItems: 'center', marginTop: 2 }}>

                            <Text style={{ width: (width - 20) / 2 - 15, fontSize: 9, fontFamily: 'Montserrat-Regular', color: 'black', textAlign: 'center' }}>SOLICITUD DE PAGO POR LA EMPRESA DE SERVICIOS DOMÉSTICOS</Text>

                            <View style={{ width: (width - 20) / 2 + 15, flexDirection: 'row', justifyContent: 'space-between' }}>

                                <TouchableOpacity style={{ width: ((width - 20) / 2) / 2 + 5, paddingHorizontal: 2, height: 30, borderRadius: 5, backgroundColor: '#006400', justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center', }}>
                                    <Image source={require('./img/money-bag.png')} style={{ width: 15, height: 15, }} />
                                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'center', marginRight: 5 }}>ACEPTAR</Text>
                                </TouchableOpacity>


                                <TouchableOpacity style={{ width: ((width - 20) / 2) / 2 + 5, height: 30, borderRadius: 5, backgroundColor: '#ffa500', justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center', }}>
                                    <Image source={require('./img/domestic-dispute.png')} style={{ width: 15, height: 15, }} />
                                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'white', textAlign: 'center', marginRight: 5 }}>DECLINAR</Text>
                                </TouchableOpacity>

                            </View>

                        </View>




                        {/* RED LINE */}
                        <View style={{ marginVertical: 3, width: width - 10 }}>
                            <Image source={require('./img/current copy 2.png')} style={{ width: width - 10, height: 4, }} />
                        </View>



                        {/* IMG CONTAINER */}
                        <View style={{ marginTop: 5, width: width - 40, height: 120, alignItems: 'center', borderRadius: 10 }}>
                            <Image source={require('./img/plumbing-issue.png')} style={{ width: width - 40, height: 120, borderRadius: 10 }} />
                        </View>





                        {/* ORDER EXECUTION */}
                        <View style={{ flexDirection: 'row', justifyContent: 'center', width: width - 40, marginTop: 10 }}>
                            <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Regular', textAlign: 'center', color: 'black' }}>ORDEN DE EJECUCIÓN POR:</Text>
                        </View>


                        <View style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: width,
                            flexDirection: 'row',
                            marginTop: 5,
                        }} >
                            <View style={{ width: (width - 20) / 4 + 20, }}>
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>EMPRESA</Text>
                            </View>
                            <View style={{ width: (width - 20) / 4 + 20, }}>
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>CLASIFICACIÓN</Text>
                            </View>
                            <View style={{ width: (width - 20) / 4 - 20, }}>
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'center', color: 'black', backgroundColor: 'transparent' }}>PRECIO</Text>
                            </View>
                            <View style={{ width: (width - 20) / 4 - 20, }}>
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', textAlign: 'right', color: 'black', backgroundColor: 'transparent' }}>REACCIÓN</Text>
                            </View>
                        </View>



                        {/* LINE */}
                        <View style={styles.line} />



                        {/* FOUR SECTIONS FIRST */}
                        <View style={styles.fourSections}>

                            <View style={[styles.fourSectionsBigContainer, { flexDirection: 'row' }]} >
                                <Image source={require('./img/profile.png')} style={{ width: 30, height: 35, flex: 3 }} />
                                <Text style={{ fontSize: 10, fontFamily: 'Montserrat-Light', color: 'black', backgroundColor: 'transparent', flexWrap: 'wrap', flex: 5 }}>Marble Arch  Repairs Ltd.</Text>
                            </View>

                            <View style={styles.fourSectionsBigContainer}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View>
                                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                    </View>
                                    <View>
                                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                    </View>
                                    <View>
                                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                    </View>
                                    <View>
                                        <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                    </View>
                                    <View>
                                        <Image source={require('./img/Shape 907 copy 4.png')} style={{ width: 15, height: 15, }} />
                                    </View>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>(129)</Text>
                                </View>
                            </View>

                            <View style={styles.fourSectionsSmallContainer}>
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>EUR149</Text>
                            </View>
                            <View style={styles.fourSectionsSmallContainer}>
                                <Text style={{ fontSize: 11, fontFamily: 'Montserrat-Light', color: 'black', textAlign: 'center', backgroundColor: 'transparent' }}>20.10.17  @17:00h</Text>
                            </View>
                        </View>


                        {/* TEXT CONTAINER */}
                        <View style={{ width: width - 40, height: 100, marginVertical: 5, backgroundColor: '#9d9db4', borderRadius: 10, justifyContent: 'center' }}>
                            <ScrollView>
                                <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'white', marginLeft: 8 }}>Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem ipsum dolor Lorem ipsum dolor sit amet, adipiscing elit, lorem  </Text>
                            </ScrollView>
                        </View>


                        {/* CHAT*/}
                        <View style={{ width: width, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginTop: 5 }}>
                            <TouchableOpacity onPress={() => {
                              this.props.navigation.navigate('Chat')
                            }}>
                                <View style={{ width: width - 100, height: 30, alignItems: 'center', justifyContent: 'center', alignItems: 'center', borderColor: 'black', borderWidth: 1, flexDirection: 'row', borderRadius: 5 }}>
                                    <Image source={require('./img/chat-icon.png')} style={{ width: 20, height: 20, marginRight: 10 }} />
                                    <Text style={{ fontSize: 12, fontFamily: 'Montserrat-Light', color: 'black' }}>CHAT CON LA COMPAÑÍA</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>

                   {/* FOOTER */}
                   <Footer icon={['user', 'home']} hightlightedIcon="home"  navigationType="navigate"
                   userPress={() => {
                     this.props.navigation.navigate('MyProfile');
                   }}
                   homePress={() => {
                     this.props.navigation.navigate('FeedUpcoming');
                   }}
                   bellPress={() => {
                     this.props.navigation.navigate('Home');
                   }}
                   />
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: width,
        height: 470
    },
    locationContainer: {
        width: 200,
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(44,57,66,0.5)',
        left: -60
    },
    location: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    line: {
        width: width - 40,
        borderBottomColor: '#999ea3',
        borderBottomWidth: 1,
    },
    fourSectionsSmallContainer: {
        width: (width - 20) / 4 - 20,
        height: 50,
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSectionsBigContainer: {
        width: (width - 20) / 4 + 20,
        height: 50,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    fourSections: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: 50,
        flexDirection: 'row',
        marginTop: 5,
    },
    footerContainer: {
        width: width,
        height: 45,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'space-around',
        alignItems: 'center'
    }

});
