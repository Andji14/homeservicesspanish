import React, { Component } from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import ServiceType from '../ServiceType/ServiceType';
import OrdersSlider from '../OrdersSlider/OrdersSlider';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ScrollView,
    FlatList

} from 'react-native';
import Dimensions from 'Dimensions';
import ProfilePicture from '../ProfilePicture/ProfilePicture';

import { ColorButton } from '../ColoredButtons';
import CalendarSlider from '../CalendarSlider/CalendarSlider';

var { height, width } = Dimensions.get('window');

export default class MyBusinessProfile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedCategory: 'limpieza',
            categories: [
                {
                    image: 'limpieza',
                    service: 'limpieza',
                },
                {
                    image: 'electrician',
                    service: 'servicios de electricista',
                },
                {
                    image: 'painter-roller',
                    service: 'pintura para pared',
                },
                {
                    image: 'question-mark',
                    service: 'otros (especificar)',
                },
            ],
            colorButtonSelected: '4 HOURS',
        }
    }
    render() {

        return (
            <View style={styles.container}>
                {/* HEADER */}
                <Header icon="menu" navigationType="title" title="MI PERFIL DE NEGOCIO" />


                {/* MAIN CONTAINER */}
                <View style={{
                    height: height - 120,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                  <ScrollView contentContainerStyle={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}>
                      {/* PROFILE INFO */}
                      <View style={{ height: 110, width: width, flexDirection: 'row', marginVertical: 5 }}>

                          <ProfilePicture
                              pictureType="filesystem"
                              picture={require('./img/image.png')}
                              name="Marble Arch Repairs Ltd."
                              cameraIcon={true}
                              numbers={478}
                              camerePress={() => { console.log('camera is pressed') }}
                          />

                          <View style={styles.topPresonalInfoContainer}>
                              <View>
                                  <Image source={require('./img/e-mail.png')} style={{ width: 30, height: 30, }} />
                              </View>
                              <View>
                                  <Image source={require('./img/location-pin.png')} style={{ width: 30, height: 30, }} />
                              </View>
                              <View>
                                  <Image source={require('./img/phone-number.png')} style={{ width: 30, height: 30, }} />
                              </View>
                          </View>


                          <View style={styles.userInfo} >
                              <Text style={styles.userInfoCoordinates}>customer@marble- arch-repairs-ltd.co.uk</Text>
                              <Text style={styles.userInfoCoordinates}>Calle Aduana,2 MADRID SPAIN[radio de operación: 120km]</Text>
                              <Text style={styles.userInfoCoordinates}>(001) 44 20 30 40</Text>
                          </View>

                          <View style={styles.topPencil} >
                              <TouchableOpacity>
                                  <Image source={require('./img/pencil-edit-button.png')} style={styles.topPencilImg} />
                              </TouchableOpacity>
                          </View>
                      </View>


                      {/* LINE */}
                      <View style={styles.line} />


                      {/* ACTIVE BANK ACCOUNT */}
                      <View>
                          <View style={styles.bankAcc}>
                              <Text style={styles.bankAccText}>CUENTA DE BANCO ACTIVO (S)</Text>
                              <TouchableOpacity style={{ position: 'absolute', right: 3, top: -1 }}>
                                  <Image source={require('./img/copy.png')} style={{ width: 30, height: 30, }} />
                              </TouchableOpacity>
                          </View>

                          <View style={styles.paymentSectionContainer}>
                              <Image source={require('./img/card.png')} style={{ width: 30, height: 25, marginHorizontal: 15 }} />
                              <TextInput
                                  autoCorrect={false}
                                  underlineColorAndroid="transparent"
                                  placeholder="**** **** ****2828 "
                                  placeholderTextColor="#FFFFFF"
                                  style={{ width: 150, textAlign: 'center', color: 'white', }}></TextInput>
                              <TouchableOpacity>
                                  <Image source={require('./img/pencil-edit-button.png')} style={styles.cardIcons} />
                              </TouchableOpacity>
                              <TouchableOpacity>
                                  <Image source={require('./img/CLOSE-WHOLE-SCREEN.png')} style={styles.cardIcons} />
                              </TouchableOpacity>
                              <TouchableOpacity>
                                  <Image source={require('./img/Checked.png')} style={[styles.cardIcons, { width: 30 }]} />
                              </TouchableOpacity>
                          </View>
                      </View>


                      {/* LINE */}
                      <View style={styles.line} />


                      {/* PROVIDED SERVICES */}
                      <View style={styles.providedServices} >
                          <Text style={styles.providedServicesText}>SERVICIOS PRESTADOS</Text>
                      </View>

                      {/* ICON CONTAINER */}
                      <View style={styles.iconsSectionContainer}>
                          <FlatList
                              horizontal={true}
                              data={this.state.categories}
                              showsHorizontalScrollIndicator={false}
                              renderItem={({ item }) => {
                                  if (item.image == this.state.selectedCategory) {
                                      return (
                                          <ServiceType
                                              color="white"
                                              onPress={() => {
                                                  this.setState({ selectedCategory: item.image, categories: this.state.categories.slice(0) });
                                              }}
                                              image={item.image}
                                              textTypeService={item.service}
                                              bold={true}
                                          />
                                      );
                                  }
                                  return (
                                      <ServiceType color="silver" onPress={() => {
                                          this.setState({ selectedCategory: item.image, categories: this.state.categories.slice(0) });
                                      }} image={item.image} textTypeService={item.service} />
                                  );
                              }}
                          />

                      </View>


                      {/* WORKERS CONTAINER */}
                      <View style={styles.professionalWorking}>

                          <View style={{ alignItems: 'center' }}>
                              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                  <Text style={styles.numbreOfWorkers} >31</Text>
                                  <Image source={require('./img/workers.png')} style={{ width: 30, height: 30, }} />
                              </View>
                              <Text style={styles.description} >TRABAJADORES</Text>
                          </View>


                          {/* FIVE STARS */}
                          <View style={{ alignItems: 'center', justifyContent: 'flex-end' }} >
                              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                                  <View>
                                      <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                  </View>
                                  <View>
                                      <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                  </View>
                                  <View>
                                      <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                  </View>
                                  <View>
                                      <Image source={require('./img/Shape 907.png')} style={{ width: 15, height: 15, }} />
                                  </View>
                                  <View>
                                      <Image source={require('./img/Shape 907 copy 4.png')} style={{ width: 15, height: 15, }} />
                                  </View>
                              </View>
                              <View>
                                  <Text style={styles.description} >(129 CLASIFICACIÓN)</Text>
                              </View>
                          </View>


                          {/* LEGAL CONTAINER */}
                          <View style={{ alignItems: 'center' }}>
                              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                  <Image source={require('./img/legal-info.png')} style={{ width: 30, height: 30, }} />
                              </View>
                              <View>
                                  <Text style={styles.description} >LEGAL</Text>
                              </View>
                          </View>

                      </View>

                      {/* LINE */}
                      <View style={styles.line} />

                      {/* UPCOMING ORDERS */}
                      <View>
                          <View style={styles.upcomingOrders}>
                              <Text style={styles.upcomingOrdersText}>PRÓXIMAS ÓRDENES PARA:</Text>
                              <TouchableOpacity style={styles.blueBotton}>
                                  <Text style={styles.blueBottonText}>16.10-22.1017</Text>
                                  <Image source={require('./img/arrow-down.png')} style={{ width: 10, height: 10, marginLeft: 2 }} />
                              </TouchableOpacity>
                          </View>
                      </View>


                      {/* COLORED BUTTONS */}
                      <View style={{ flexDirection: 'row' }}>

                          <View style={styles.bottomContainer}>

                            <ColorButton
                              color="#ff0000"
                              selected={this.state.colorButtonSelected == "4 HOURS" }
                              text="4 HORAS"
                              onPress={() => this.setState({colorButtonSelected: "4 HOURS"})}
                            />
                            <ColorButton
                              color="#ffa500"
                              selected={this.state.colorButtonSelected == "ASAP" }
                              text="ASAP"
                              onPress={() => this.setState({colorButtonSelected: "ASAP"})}
                            />
                            <ColorButton
                              color="#006b14"
                              selected={this.state.colorButtonSelected == "NON-URGENT" }
                              text="NO URGENTE"
                              onPress={() => this.setState({colorButtonSelected: "NON-URGENT"})}
                            />
                            <ColorButton
                              color="#2943fb"
                              selected={this.state.colorButtonSelected == "VAR.HOME REPAIRS" }
                              text="REPARACIONES EN EL HOGAR"
                              onPress={() => this.setState({colorButtonSelected: "VAR.HOME REPAIRS"})}
                            />

                          </View>


                          {/* MONTHS AND ORDER CONTAINER */}
                          <View style={styles.monthsAndOrderContainer}>

                              {/* MONTHS */}
                              <CalendarSlider />

                              {/* BILL */}
                              <View>

                                  {/* ORDER INFO*/}
                                  <OrdersSlider leftColor='#0dbff2' shadowColor="#B00702" />
                              </View>
                          </View>
                      </View>
                  </ScrollView>
                </View>
                {/* FOOTER */}
                <Footer icon={['user', 'home']} hightlightedIcon="user" navigationType="navigate"
                  homePress={() => {
                    this.props.navigation.navigate('FeedUpcoming');
                  }}
                  userPress={() => {
                    this.props.navigation.navigate('MyProfile');
                  }}
                  bellPress={() => {
                    this.props.navigation.navigate('Home');
                  }}
                />

            </View >

        );
    }
}
const styles = StyleSheet.create({
    NumberInImage: {
        fontFamily: 'Montserrat-Regular',
        color: 'white',
        position: 'absolute',
        top: 3,
        right: 10,
        backgroundColor: 'rgba(38,32,37,0.5)',
        borderRadius: 5,
        paddingHorizontal: 2
    },
    camera: {
        resizeMode: 'contain',
        height: 25,
        position: 'absolute',
        top: 0,
        left: 0
    },
    clickForBill: {
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center'
    },
    orderMonthAndName: {
        width: 90,
        height: 80,
        fontSize: 13,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center',
    },
    orderInfo: {
        width: 220,
        height: 75,
        borderLeftColor: '#0dbff2',
        borderRightColor: '#323c46',
        borderTopColor: '#323c46',
        borderBottomColor: '#323c46',
        borderWidth: 3,
        margin: 5,
        backgroundColor: '#445059'
    },
    monthTextStyle: {
        color: '#989ea3',
        marginRight: 2,
        textAlign: 'center',
        fontFamily: 'Montserrat-Light'
    },
    monthCont: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
    },
    monthsAndOrderContainer: {
        width: width - 100,
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center'
    },
    blueBtn: {
        width: 80,
        height: 22,
        borderRadius: 15,
        backgroundColor: '#2943fb',
        justifyContent: 'center'
    },
    greenBtn: {
        width: 80,
        height: 22,
        borderRadius: 15,
        backgroundColor: '#006b14',
        justifyContent: 'center'
    },
    coloredButton: {
        width: 80,
        height: 22,
        borderRadius: 15,
        justifyContent: 'center'
    },

    bottomContainer: {
        alignItems: 'center',
        justifyContent: 'space-around',
        marginHorizontal: 3,
        borderRightColor: '#02bff3',
        borderTopColor: '#323c46',
        borderLeftColor: '#323c46',
        borderBottomColor: '#323c46',
        borderWidth: 2,
    },
    blueBottonText: {
        fontSize: 11,
        fontFamily: 'Montserrat-Light',
        color: 'white'
    },
    blueBotton: {
        width: 110,
        height: 25,
        borderRadius: 15,
        backgroundColor: '#004ca0',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    upcomingOrdersText: {
        fontSize: 14,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        marginRight: 5
    },
    upcomingOrders: {
        width: width - 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        marginTop: 5
    },
    description: {
        fontSize: 10,
        fontFamily: 'Montserrat-SemiBold',
        color: 'white',
        justifyContent: 'center',
        textAlign: 'center'
    },
    numbreOfWorkers: {
        fontSize: 12,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        fontWeight: 'bold'
    },
    textTypeService: {
        fontSize: 10,
        fontFamily: 'Montserrat-Light',
        color: '#7f858c',
        width: 65,
        height: 35,
        textAlign: 'center'
    },
    serciceSmallContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 80,
        marginHorizontal: 3,
        flexDirection: 'row',
        backgroundColor: 'green'
    },
    providedServicesText: {
        marginVertical: 3,
        fontSize: 14,
        fontFamily: 'Montserrat-Light',
        color: 'white',
    },
    providedServices: {
        width: width - 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
    },
    cardIcons: {
        width: 25,
        height: 25,
        marginHorizontal: 2
    },
    bankAccText: {
        marginVertical: 3,
        fontSize: 14,
        fontFamily: 'Montserrat-Light',
        color: 'white',
        textAlign: 'center'
    },
    bankAcc: {
        width: width - 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
    },
    line: {
        width: width - 60,
        borderBottomColor: '#999ea3',
        borderBottomWidth: 1,
    },
    topPencilImg: {
        width: 30,
        height: 30,
        borderColor: '#999ea3',
        borderWidth: 1
    },
    topPencil: {
        width: 30,
        height: 110,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    userInfoCoordinates: {
        fontSize: 10,
        fontFamily: 'Montserrat-Light',
        color: 'white',
    },
    userInfo: {
        width: 120,
        height: 110,
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 5
    },
    topPresonalInfoContainer: {
        width: 35,
        height: 110,
        flexDirection: 'column',
        borderRightColor: '#02bff3',
        borderTopColor: '#323c46',
        borderLeftColor: '#323c46',
        borderBottomColor: '#323c46',
        borderWidth: 2,
        justifyContent: 'space-around',
        alignContent: 'center',

    },
    texiInImage: {
        width: 100,
        textAlign: 'center',
        fontSize: 12,
        fontFamily: 'Montserrat-Bold',
        color: 'white',
        position: 'absolute',
        top: 70,
        left: 15,
        backgroundColor: 'rgba(38,32,37,0.5)'
    },

    businessProfile: {
        fontSize: 18,
        fontFamily: 'Montserrat-Light',
        color: 'white',
    },
    homeButt: {
        position: 'absolute',
        width: width,
        height: height,
        top: 0,
        left: 0
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#323c46',

    },
    headerConainer: {
        width: width,
        height: 50,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#3fabbe',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20

    },
    paymentSectionContainer: {
        width: width - 40,
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    iconsSectionContainer: {
        width: width - 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    professionalWorking: {
        width: width - 20,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

});
